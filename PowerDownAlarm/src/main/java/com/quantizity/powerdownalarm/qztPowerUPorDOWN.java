package com.quantizity.powerdownalarm;

import android.app.AlarmManager;
import android.app.PendingIntent;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;

/**
 * Created by paco on 29/09/13.
 */
public class qztPowerUPorDOWN extends BroadcastReceiver {
        @Override
        public void onReceive(Context context, Intent intent) {
            if (intent.getAction().equals(Intent.ACTION_BOOT_COMPLETED)) {
                Intent i = new Intent(context, qztPowerUPorDOWNService.class);
                i.putExtra("wakeup", true);
                context.startService(i); // start service to register receiver
            }
            else if (intent.getAction().equals(Intent.ACTION_POWER_CONNECTED)) {
                long tiempo;
                String telefono = "";
                String activado="";
                String cancelado="";
                qztXMLFile qztFile = new qztXMLFile(context.getResources().getString(R.string.nombreFichero), context);
                activado = qztFile.leeActivado();
                tiempo = qztFile.leeTiempo();
                telefono = qztFile.leeTelefono();
                cancelado=qztFile.leeCancelado();
                qztFile.grabaParametros(tiempo, tiempo, telefono, "-1", cancelado);

                AlarmManager almMgr = (AlarmManager) context.getSystemService(Context.ALARM_SERVICE);
                PendingIntent pi = PendingIntent.getService(context, 0, new Intent (context, qztNotifyPowerDownService.class), PendingIntent.FLAG_UPDATE_CURRENT);
                almMgr.cancel(pi);

            }
            else if (intent.getAction().equals(Intent.ACTION_POWER_DISCONNECTED)) {
                long tiempo;
                String telefono = "";
                String activado="";
                String cancelado="";
                qztXMLFile qztFile = new qztXMLFile(context.getResources().getString(R.string.nombreFichero), context);
                activado = qztFile.leeActivado();
                tiempo = qztFile.leeTiempo();
                telefono = qztFile.leeTelefono();
                cancelado = qztFile.leeCancelado();
                qztFile.grabaParametros(tiempo, tiempo, telefono, "1", cancelado);
                AlarmManager almMgr = (AlarmManager) context.getSystemService(Context.ALARM_SERVICE);
                PendingIntent pi = PendingIntent.getService(context, 0, new Intent (context, qztNotifyPowerDownService.class),PendingIntent.FLAG_UPDATE_CURRENT);
                almMgr.set(AlarmManager.RTC_WAKEUP,  System.currentTimeMillis() + (60 * 1000 * tiempo), pi);
            }

        }
    }