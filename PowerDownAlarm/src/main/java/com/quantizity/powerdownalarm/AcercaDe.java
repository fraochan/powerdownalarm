package com.quantizity.powerdownalarm;

import android.app.Activity;
import android.content.pm.PackageInfo;
import android.os.Bundle;
import android.widget.TextView;

/**
 * Created by paco on 21/10/13.
 */
public class AcercaDe extends Activity {
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.acerca_de);
        String version = "";
        PackageInfo info = null;
        TextView text = (TextView) findViewById(R.id.aboutText);
        try {
            info = getPackageManager().getPackageInfo(getPackageName(), 0);
            version = info.versionName;
        } catch (Exception e) {
            version = "1.0";
        }
        text.setText(String.format(getResources().getString(R.string.about_text),version));

    }
}
