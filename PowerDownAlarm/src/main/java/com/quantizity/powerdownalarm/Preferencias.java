package com.quantizity.powerdownalarm;

import android.os.Bundle;
import android.preference.PreferenceActivity;

/**
 * Created by paco on 21/10/13.
 */
public class Preferencias extends PreferenceActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        // Load the preferences from an XML resource
        addPreferencesFromResource(R.xml.preferencias);
    }
}
