package com.quantizity.powerdownalarm;

import android.app.AlarmManager;
import android.app.IntentService;
import android.app.PendingIntent;
import android.app.Service;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.BatteryManager;
import android.os.IBinder;

/**
 * Created by paco on 29/09/13.
 */
public class qztPowerUPorDOWNService extends IntentService {
    private BroadcastReceiver receiver = null;
    boolean wakeup = false;

    public qztPowerUPorDOWNService() {
        super("qztPowerUPorDOWNService");
    }

    @Override
    public IBinder onBind(Intent arg0) {
        return null;
    }


    @Override
    public void onCreate() {
        super.onCreate();
        if(receiver==null) {
            receiver = new qztPowerUPorDOWN();
            IntentFilter filter = new IntentFilter();
            filter.addAction(Intent.ACTION_POWER_CONNECTED);
            filter.addAction(Intent.ACTION_POWER_DISCONNECTED);
            filter.addAction(Intent.ACTION_BOOT_COMPLETED);
            registerReceiver(receiver, filter);
        }



    }

    @Override
    //public int onStartCommand(Intent intent, int flags, int startId) {
        //super.onStartCommand(intent, flags, startId);
    protected void onHandleIntent(Intent intent) {
        //super.onHandleIntent(intent);
        IntentFilter ifilter = new IntentFilter(Intent.ACTION_BATTERY_CHANGED);
        Intent batteryStatus = this.registerReceiver(null, ifilter);
        int status = batteryStatus.getIntExtra("status", -1);
        boolean isCharging = status == BatteryManager.BATTERY_STATUS_CHARGING ||status == BatteryManager.BATTERY_STATUS_FULL;
        long tiempo;
        String telefono = "";
        String cancelado = "";
        qztXMLFile qztFile = new qztXMLFile(this.getResources().getString(R.string.nombreFichero), this);

        if (intent.getExtras()!=null && intent.getExtras().getBoolean("wakeup")) {
            wakeup = true;
        }

        if (qztFile!=null) {
            tiempo = qztFile.leeTiempo();
            telefono = qztFile.leeTelefono();
            cancelado = qztFile.leeCancelado();

            if (tiempo>0 && telefono!=null && cancelado!=null) {
                if (cancelado.equals("1")) {
                    stopSelf();
                }else if (!isCharging && wakeup) {

                     qztFile.grabaParametros(tiempo, tiempo, telefono, "1", cancelado);
                     AlarmManager almMgr = (AlarmManager) this.getSystemService(Context.ALARM_SERVICE);
                     PendingIntent pi = PendingIntent.getService(this, 0, new Intent (this, qztNotifyPowerDownService.class),PendingIntent.FLAG_UPDATE_CURRENT);
                     almMgr.set(AlarmManager.RTC_WAKEUP,  System.currentTimeMillis() + (60 * 1000 * tiempo), pi);
                }
            }
            else {
                stopSelf();
            }
        }

        //return START_STICKY;
    }

    @Override
    public void onDestroy(){
        unregisterReceiver(receiver);
        super.onDestroy();
    }

}
