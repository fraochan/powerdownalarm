package com.quantizity.powerdownalarm;

import android.content.Context;
import android.util.Xml;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.StringWriter;

/*import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;*/

import org.xml.sax.*;
import org.xml.sax.helpers.DefaultHandler;
import org.xmlpull.v1.XmlSerializer;

import javax.xml.parsers.SAXParser;
import javax.xml.parsers.SAXParserFactory;

/**
 * Created by paco on 05/10/13.
 */
public class qztXMLFile  {
    String name;
    Context context = null;
    FileInputStream in = null;
    FileOutputStream out = null;
    XMLHandler myXMLHandler = null;

    qztXMLFile(String nombre, Context ctx) {
        this.name = nombre;
        this.context = ctx;
        try {
            SAXParserFactory saxPF = SAXParserFactory.newInstance();
            SAXParser saxP = saxPF.newSAXParser();
            XMLReader xmlR = saxP.getXMLReader();


            myXMLHandler = new XMLHandler();
            xmlR.setContentHandler(myXMLHandler);
            xmlR.parse(new InputSource(context.openFileInput(name)));

        } catch (Exception e) {
            System.out.println(e);
        }
    }

    private void abreFicheroLectura() {
        try {
            in = context.openFileInput(name);
        } catch (FileNotFoundException e) {
            System.err.println("No se encuentra el fichero");
            e.printStackTrace();
        }

    }

    private void cierraFicheroLectura() {
        if (in!=null) {
            try {
                in.close();
            } catch (IOException e) {
                System.err.println("Error IO");
                e.printStackTrace();
            }
        }
    }


    private void abreFicheroEscritura() {
        try {
            out = context.openFileOutput(name, context.MODE_PRIVATE);
        } catch (FileNotFoundException e) {
            System.err.println("No se encuentra el fichero");
            e.printStackTrace();
        }
    }

    private void cierraFicheroEscritura() {
        if (out!=null) {
            try {
                out.close();
            } catch (IOException e) {
                System.err.println("Error IO");
                e.printStackTrace();
            }
        }
    }
    public class XMLHandler extends DefaultHandler {

        long tiempo;
        long pendiente;

        public String getTelefono() {
            return telefono;
        }

        public void setTelefono(String telefono) {
            this.telefono = telefono;
        }

        public String getActivado() {
            return activado;
        }

        public void setActivado(String activado) {
            this.activado = activado;
        }

        public long getPendiente() {
            return pendiente;
        }

        public void setPendiente(long pendiente) {
            this.pendiente = pendiente;
        }

        public long getTiempo() {
            return tiempo;
        }

        public void setTiempo(long tiempo) {
            this.tiempo = tiempo;
        }

        String activado;
        String telefono;

        public String getCancelado() {
            return cancelado;
        }

        public void setCancelado(String cancelado) {
            this.cancelado = cancelado;
        }

        String cancelado;
        @Override
        public void startElement(String uri, String localName, String qName,
                                 Attributes attributes) throws SAXException {

            if (localName.equals("Set")) {
                setTelefono(attributes.getValue("telefono"));
                setActivado(attributes.getValue("activado"));
                setCancelado(attributes.getValue("cancelado"));
                setTiempo(Long.valueOf(attributes.getValue("tiempo").equals("") ? "0" : attributes.getValue("tiempo")).longValue());
                setPendiente(Long.valueOf(attributes.getValue("pendiente").equals("") ? "0" : attributes.getValue("pendiente")).longValue());
            }
        }

        @Override
        public void endElement(String uri, String localName, String qName)
                throws SAXException {

        }

        @Override
        public void characters(char[] ch, int start, int length)
                throws SAXException {

        }

        private String writeXml(Long tiempo, Long pendiente, String telefono, String activado, String cancelado){
            XmlSerializer serializer = Xml.newSerializer();
            StringWriter writer = new StringWriter();
            try {
                serializer.setOutput(writer);
                serializer.startDocument("UTF-8", true);
                serializer.startTag("", "Sets");
                serializer.startTag("", "Set");
                serializer.attribute("", "tiempo", tiempo.toString());
                serializer.attribute("", "pendiente", pendiente.toString());
                serializer.attribute("", "telefono", telefono == null ? "" : telefono);
                serializer.attribute("", "activado", activado == null ? "" : activado);
                serializer.attribute("", "cancelado", cancelado == null ? "" : cancelado);
                serializer.endTag("", "Set");
                serializer.endTag("", "Sets");
                serializer.endDocument();
                return writer.toString();
            } catch (Exception e) {
                throw new RuntimeException(e);
            }
        }

    }


    public void grabaParametros(Long tiempo, Long pendiente, String telefono, String activado, String cancelado) {
        abreFicheroEscritura();
        if (out!=null) {
          try {
              out.write(myXMLHandler.writeXml(tiempo, pendiente, telefono, activado, cancelado).getBytes());
          } catch (Exception ex) {
              ex.printStackTrace();
          }
            /*DocumentBuilderFactory documentBuilderFactory = DocumentBuilderFactory.newInstance();
            try {
                DocumentBuilder documentBuilder = documentBuilderFactory.newDocumentBuilder();
                Document document = documentBuilder.newDocument();

                Element root = document.createElement("Sets");

                Element elSet = document.createElement("Set");
                elSet.setAttribute("tiempo", tiempo.toString());
                elSet.setAttribute("telefono", telefono == null ? "" : telefono);
                elSet.setAttribute("Activado", activado == null ? "" : activado);
                elSet.setAttribute("pendiente", pendiente.toString());
                elSet.setAttribute("cancelado", cancelado == null ? "" : cancelado);
                root.appendChild(elSet);

                TransformerFactory transformerFactory = TransformerFactory.newInstance();
                Transformer transformer = transformerFactory.newTransformer();
                DOMSource source = new DOMSource(root);
                StreamResult result =  new StreamResult(out);
                transformer.transform(source, result);

            } catch (ParserConfigurationException e) {
                e.printStackTrace();
            } catch (TransformerException e) {
                e.printStackTrace();
            }*/
        }
        cierraFicheroEscritura();
    }

    public long leeTiempo() {
        /*long tiempo = -1;
        tiempo=Long.valueOf(leeParametro("tiempo").equals("") ? "0" : leeParametro("tiempo")).longValue();*/
        return myXMLHandler.getTiempo();
    }

    public long leePendiente() {
        /*long pendiente = -1;
        pendiente = Long.valueOf(leeParametro("pendiente").equals("") ? "0" : leeParametro("pendiente")).longValue();*/
        return myXMLHandler.getPendiente();
    }

    public String leeTelefono() {
        /*String telefono = "";
        telefono = leeParametro("telefono");*/
        return myXMLHandler.getTelefono();
    }

    public String leeActivado() {
        /*String activado = "";
        activado = leeParametro("Activado");*/
        return myXMLHandler.getActivado();
    }

    public String leeCancelado() {
        /*String cancelado = "";
        cancelado = leeParametro("cancelado");*/
        return myXMLHandler.getCancelado();
    }

    private String leeParametro(String nombre)
    {
        String res="";
        abreFicheroLectura();
        if (in!=null) {




        /*    try {
                DocumentBuilderFactory documentBuilderFactory = DocumentBuilderFactory.newInstance();
                DocumentBuilder documentBuilder = documentBuilderFactory.newDocumentBuilder();
                Document document = documentBuilder.parse(in);

                NodeList nSets = document.getElementsByTagName("Set");

                for (int i=0; i<nSets.getLength(); i++) {
                    Node elSet = nSets.item(i);
                    if (elSet.getNodeType() == Node.ELEMENT_NODE) {
                        res = elSet.getAttributes().getNamedItem(nombre).getNodeValue();
                    }
                }
            } catch (IOException e) {
                e.printStackTrace();
            } catch (ParserConfigurationException e) {
                System.err.println("Error parser");
                e.printStackTrace();
            } catch (SAXException e) {
                System.err.println("Error en SAX");
                e.printStackTrace();
            }*/
        }
        cierraFicheroLectura();
        return res;
    }
}
