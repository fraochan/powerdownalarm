package com.quantizity.powerdownalarm;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.Toast;

public class qztPowerDownAlarm extends Activity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_config);
        Spinner cTiempos = (Spinner) findViewById(R.id.cTiempos);
        ArrayAdapter<CharSequence> adapter = ArrayAdapter.createFromResource(this, R.array.tiempos, android.R.layout.simple_spinner_item);
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        cTiempos.setAdapter(adapter);

        long tiempo=1;
        String telefono="";
        String activado="";
        qztXMLFile qztFile = new qztXMLFile(this.getResources().getString(R.string.nombreFichero), this);
        telefono = qztFile.leeTelefono();
        tiempo = qztFile.leeTiempo();
        activado = qztFile.leeActivado();


        EditText editPhone = (EditText) findViewById(R.id.sTelefono);
        editPhone.setText(telefono);
        if (tiempo==5) {
            cTiempos.setSelection(0);
        } else if (tiempo==15) {
            cTiempos.setSelection(1);
        } else if (tiempo==30) {
            cTiempos.setSelection(2);
        } else if (tiempo==45) {
            cTiempos.setSelection(3);
        } else if (tiempo==60) {
            cTiempos.setSelection(4);
        }

    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.qzt_power_down_alarm, menu);
        return true;
    }

    public void saveAndClose(View view) {
        Intent i = new Intent(this, qztPowerUPorDOWNService.class);
        i.putExtra("wakeup", false);
        startService(i);
        long tiempo = 1;
        String telefono="";
        EditText eTelefono = (EditText) findViewById(R.id.sTelefono);
        telefono = eTelefono.getText().toString();
        Spinner spinner = (Spinner) findViewById(R.id.cTiempos);
        int pos = spinner.getSelectedItemPosition();
        if (pos==0) {
            tiempo=5;
        } else if (pos==1) {
            tiempo=15;
        } else if (pos==2) {
            tiempo=30;
        } else if (pos==3) {
            tiempo=45;
        } else if (pos==4) {
            tiempo=60;
        }

        qztXMLFile qztFile = new qztXMLFile(this.getResources().getString(R.string.nombreFichero), this);
        qztFile.grabaParametros(tiempo, tiempo, telefono, "-1", "0");

        this.finish();
    }

    public void stopAndClose(View view) {

        qztXMLFile qztFile = new qztXMLFile(this.getResources().getString(R.string.nombreFichero), this);
        qztFile.grabaParametros(5L, 5L, "", "-1", "1");

        stopService(new Intent(this, qztPowerUPorDOWNService.class));
        this.finish();
    }

    public void mostrarConfig(View view) {
        long tiempo=0;
        long pendiente=0;
        String telefono="";
        String activado="";
        qztXMLFile qztFile = new qztXMLFile(this.getResources().getString(R.string.nombreFichero), this);
        telefono = qztFile.leeTelefono();
        tiempo = qztFile.leeTiempo();
        activado = qztFile.leeActivado();
        pendiente = qztFile.leePendiente();
        Toast.makeText(this, "Tiempo: " + tiempo + " - Telefono: " + telefono + " - Activado: " + activado + " - Pendiente: " + pendiente, Toast.LENGTH_LONG).show();
    }

    public void lanzarAcercaDe(View view) {
        Intent i = new Intent (this, AcercaDe.class);
        startActivity(i);
    }

    public void lanzarPreferencias(View view) {
        Intent i = new Intent(this, Preferencias.class);
        startActivity(i);
    }

    public void lanzarInstrucciones(View view) {
        Intent i = new Intent(this, Instrucciones.class);
        startActivity(i);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch(item.getItemId()) {
            case R.id.action_settings:
                lanzarPreferencias(null);
                break;
            case R.id.action_about:
                lanzarAcercaDe(null);
                break;
            case R.id.action_instructions:
                lanzarInstrucciones(null);
                break;
        }
        return true;
    }
}
