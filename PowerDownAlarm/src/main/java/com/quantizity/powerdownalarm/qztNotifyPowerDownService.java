package com.quantizity.powerdownalarm;

/**
 * Created by paco on 05/10/13.
 */

import android.app.IntentService;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.IBinder;
import android.preference.PreferenceManager;
import android.support.v4.app.NotificationCompat;
import android.telephony.SmsManager;

public class qztNotifyPowerDownService extends Service {


    @Override
    public IBinder onBind(Intent arg0) {
        return null;
    }

    @Override
    public void onCreate() {
        super.onCreate();


        long tiempo;
        long pendiente;
        String telefono = "";
        String activado="";
        qztXMLFile qztFile = new qztXMLFile(getApplicationContext().getResources().getString(R.string.nombreFichero), getApplicationContext());
        activado = qztFile.leeActivado();
        tiempo = qztFile.leeTiempo();
        pendiente = qztFile.leePendiente();
        telefono = qztFile.leeTelefono();

        if (activado.equals("1") && !telefono.equals("")) {
            SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(getApplicationContext());

            if (prefs.getBoolean("test",false)) {
                String texto = prefs.getString("smsText", "").equals("") ? getApplicationContext().getResources().getString(R.string.smsMessage) : prefs.getString("smsText", "");
                NotificationCompat.Builder mBuilder =
                        new NotificationCompat.Builder(getApplicationContext())
                                .setSmallIcon(R.drawable.ic_launcher)
                                .setContentTitle(telefono)
                                .setContentText(texto);
                Intent resultIntent = new Intent(getApplicationContext(), qztPowerDownAlarm.class);
                PendingIntent resultPendingIntent =
                        PendingIntent.getActivity(
                                getApplicationContext(),
                                0,
                                resultIntent,
                                PendingIntent.FLAG_UPDATE_CURRENT
                        );
                mBuilder.setContentIntent(resultPendingIntent);
                NotificationManager nm;
                nm = (NotificationManager)getApplicationContext().getSystemService(Context.NOTIFICATION_SERVICE);
                nm.notify(001, mBuilder.build());
            }
            else {
                SmsManager smsManager = SmsManager.getDefault();
                String texto = prefs.getString("smsText", "").equals("") ? getApplicationContext().getResources().getString(R.string.smsMessage) : prefs.getString("smsText", "");
                smsManager.sendTextMessage(telefono, null,texto, null, null);
            }
            qztFile.grabaParametros(tiempo, tiempo, telefono, "1", "0");
        }

        stopSelf();

    }

    /*@Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        super.onStartCommand(intent, flags, startId);
        return START_NOT_STICKY;
    }*/



    @Override
    public void onDestroy(){
        super.onDestroy();
    }
}
